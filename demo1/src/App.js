import logo from "./logo.svg";
import React, { useState } from "react";
import "./App.css";

export default function App() {
  //props => { setCount, count, btnTitle, resultMessage }
  const CountMe = ({ setCount, count, btnTitle, resultMessage }) => {
    return (
      <>
        <button
          onClick={(e) => {
            setCount(count + 1);
            console.log(count);
          }}
        >
          {btnTitle}
        </button>
        &nbsp;
        <span>
          {resultMessage} {count}
        </span>
        <br />
      </>
    );
  };

  const Header = (props) => {
    return (
      <div>
        <h1>{props.title}</h1>
      </div>
    );
  };

  const Footer = (props) => {
    return (
      <div>
        <h1>{props.footer}</h1>
      </div>
    );
  };

  var count1 = 0;
  const [count2, setCount2] = React.useState(0);
  const [count3, setCount3] = useState(1);

  return (
    <div>
      <Header title="hey google" />
      <Footer footer="this is footer" />
      <button
        onClick={(e) => {
          count1 = count1 + 1;
          console.log(count1);
        }}
      >
        Click me!
      </button>
      <br />
      <span>Count 1 = {count1}</span>

      <br />
      <br />

      <button
        onClick={(e) => {
          setCount2(count2 + 1); //Add
          console.log(count2);
        }}
      >
        Click me 2 !
      </button>
      <br />
      <span>Count 2 = {count2}</span>

      <br />
      <br />

      <CountMe
        btnTitle="ClickMe"
        resultMessage="Count3 is : "
        setCount={setCount3}
        count={count3}
      />
    </div>
  );
}
